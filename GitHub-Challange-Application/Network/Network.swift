

import Combine
import Foundation

final class Network {
    func getRequest() -> AnyPublisher<GitHubModel, ServiceError> {
        var url: URL {
            var components = URLComponents()
            components.scheme = "https"
            components.host = "api.github.com"
            components.path = "/search/repositories"
            components.queryItems = [
                URLQueryItem(name: "q", value: "topic:android"),
                URLQueryItem(name: "sort", value: "stars"),
                URLQueryItem(name: "order", value: "desc")
            ]
            guard let url = components.url else {
                preconditionFailure("Invalid URL components: \(components)")
            }
            return url
        }

        var request = URLRequest(url: url)
        request.addValue("application/vnd.github.v3+json", forHTTPHeaderField: "Accept")
        request.addValue("token 2de38bb65feb853056f3a70f8684bb3aa78dc832", forHTTPHeaderField: "Authorization")

        return URLSession.shared
            .dataTaskPublisher(for: request)
            .receive(on: DispatchQueue.main)
            .mapError { _ in ServiceError.unknown }
            .flatMap { data, response -> AnyPublisher<GitHubModel, ServiceError> in
                if let response = response as? HTTPURLResponse {
                    if (200 ... 299).contains(response.statusCode) {
                        return Just(data)
                            .decode(type: GitHubModel.self, decoder: JSONDecoder())
                            .mapError { _ in .decodingError }
                            .eraseToAnyPublisher()
                    } else {
                        return Fail(error: ServiceError.httpError(response.statusCode))
                            .eraseToAnyPublisher()
                    }
                }
                return Fail(error: ServiceError.unknown)
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}

enum ServiceError: Error {
    case decodingError
    case httpError(Int)
    case unknown
}

extension URL {}
