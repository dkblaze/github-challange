//
//  GitHub_Challange_ApplicationApp.swift
//  GitHub-Challange-Application
//
//  Created by Chris on 2/21/21.
//

import SwiftUI

@main
struct GitHub_Challange_ApplicationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
