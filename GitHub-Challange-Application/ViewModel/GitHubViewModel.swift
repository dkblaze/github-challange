


import Combine
import Foundation


final class GitHubViewModel: ObservableObject {
    
    @Published  var data: GitHubModel?
    private let network = Network()
    private var cancellable: AnyCancellable?
    
    init(){
        self.fetchData()
    }
    
    func fetchData() {
        DispatchQueue.main.async {
            self.cancellable =   self.network.getRequest()
                .sink(receiveCompletion: { _ in }, receiveValue: { container in
                    self.data = container
                    print(self.data!)
                })
        }
    }

}
