

import Combine
import Foundation

struct GitHubModel: Codable,Hashable {
   
    let items: [Profiles]
}

struct Profiles: Codable,Hashable,Identifiable {
    let id = UUID()
    let name: String
    let fullName: String
    let description: String
    let owner: Owner
    let repoURL: String
    let stars: Int
    let watchers: Int
    let forks: Int

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case fullName = "full_name"
        case description
        case owner
        case repoURL = "html_url"
        case stars = "stargazers_count"
        case watchers = "watchers_count"
        case forks = "forks_count"
    }
}

struct Owner: Codable,Hashable {
    let login: String
}
