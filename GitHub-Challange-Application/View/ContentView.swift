


import SwiftUI

struct ContentView: View {
    @ObservedObject var reposData = GitHubViewModel()

    var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(reposData.data?.items ?? []) { item in

                        HStack {
                            Text(item.name)

                            NavigationLink(
                                destination: InfoView(name: item.name, owner: item.owner.login, watchers: item.watchers, forks: item.forks, decription: item.description, url: item.repoURL),
                                label: {
                                    HStack{
                                    Text("\(item.stars)")
                                    Image(systemName: "star.fill")
                                        .foregroundColor(.red)
                                    }.frame(maxWidth: .infinity, alignment: .trailing)

                                })

                        }.font(.system(size: 15, design: .rounded))
                        .lineLimit(3)
                    }
                }
            }.navigationTitle("Android repos")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
