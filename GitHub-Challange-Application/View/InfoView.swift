

import SwiftUI

struct InfoView: View {
    var name:String
    var owner:String
    var watchers:Int
    var forks:Int
    var decription:String
    var url:String
    
    var body: some View {
        VStack(alignment: .center) {
            Text("Name - \(name)")
                .font(.system(size: 32, weight: .bold, design: .rounded))
                .padding(.bottom, 25)
            
            Text("Owner - \(owner)")
                .font(.system(size: 18, weight: .semibold, design: .rounded))
                .foregroundColor(/*@START_MENU_TOKEN@*/ .blue/*@END_MENU_TOKEN@*/)
                .padding(.bottom, 35)
                
            HStack {
                Text("Watcher - ")
                Text("\(watchers)")
            }.padding(.bottom, 2)
                
            HStack {
                Text("Forks - ")
                Text("\(forks)")
            }.padding(.bottom, 25)
            
            Text("\(decription)")
                .padding(.bottom,50)
                .padding(.leading,20)
                .font(.system(size: 18, weight: .regular, design: .rounded))
                
            Text("URL - \(url)")
                .font(.system(size: 13, weight: .bold, design: .rounded))
                .foregroundColor(.red)
            Spacer()
        }
    }
}

